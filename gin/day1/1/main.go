package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

func sayHello(w http.ResponseWriter, r *http.Request) {
	data, _ := ioutil.ReadFile("./hello.html")
	_, _ = fmt.Fprintln(w, string(data))
}
func main() {
	http.HandleFunc("/hello", sayHello)
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		fmt.Println("http server failed", err)
		return
	}
}
