package main

import (
	"fmt"
	"html/template"
	"net/http"
)

func sayHello(w http.ResponseWriter, r *http.Request) {

	// 2 解析模板
	t, err := template.ParseFiles("./hello.tmpl")
	if err != nil {
		fmt.Println("Parse template failed, err:", err)
	}

	// 3 渲染模板
	name := "模板引擎初体验"
	t.Execute(w, name)
	if err != nil {
		fmt.Println("render template failed, err:", err)
	}
}
func main() {

	http.HandleFunc("/", sayHello)

	err := http.ListenAndServe(":9000", nil)

	if err != nil {
		fmt.Println("http server failed error: ", err)
	}
}
