package main

import (
	"fmt"
	"html/template"
	"net/http"
)

type User struct {
	Name   string
	Gender string
	Age    int
}

func sayHello(w http.ResponseWriter, r *http.Request) {
	// 定义模板
	// 解析模板
	t, err := template.ParseFiles("./hello.tmpl")
	if err != nil {
		fmt.Println("解析模板失败 err:", err)
		return
	}
	// 渲染模板
	u1 := User{
		Name:   "小王子",
		Gender: "男",
		Age:    18,
	}
	// err = t.Execute(w, u1)
	// if err != nil {
	// 	fmt.Println("模板渲染失败 err:", err)
	// 	return
	// }

	m1 := map[string]interface{}{
		"name":   "大王子",
		"gender": "男",
		"age":    20,
	}
	// err = t.Execute(w, m1)
	// if err != nil {
	// 	fmt.Println("模板渲染失败 err:", err)
	// 	return
	// }
	hobbyList := []string{
		"篮球",
		"足球",
		"双色球",
	}
	err = t.Execute(w, map[string]interface{}{
		"u1":    u1,
		"m1":    m1,
		"hobby": hobbyList,
	})
	if err != nil {
		fmt.Println("模板渲染失败 err:", err)
		return
	}
}
func main() {
	http.HandleFunc("/", sayHello)

	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		fmt.Println("Http server failed error:", err)
	}
}
