package main

import (
	"fmt"
	"html/template"
	"net/http"
)

func sayHello(w http.ResponseWriter, r *http.Request) {
	// 定义一个函数kua
	// 要么只有一个返回值，要么有两个返回值，第二个返回值必须是error类型
	k := func(name string) (string, error) {
		return name + "年轻又帅气！", nil
	}

	// 通过自己新建模板对象f
	t := template.New("f.tmpl") //创建一个名字是f的模板对象，名字一定要与模板的名字能对应上
	// 告诉模板引擎，我现在多了一个自定义的函数k
	t.Funcs(template.FuncMap{
		"kua": k,
	})

	_, err := t.ParseFiles("./f.tmpl")
	if err != nil {
		fmt.Println("模板解析失败， err", err)
		return
	}
	t.Execute(w, "hello")
}

func demo1(w http.ResponseWriter, r *http.Request) {
	// 定义模板
	// 解析模板
	t, err := template.ParseFiles("./t.tmpl", "ul.tmpl")
	if err != nil {
		fmt.Println("解析模板失败", err)
		return
	}
	// 渲染模板
	name := "小王子"
	t.Execute(w, name)
}
func main() {
	http.HandleFunc("/", sayHello)
	http.HandleFunc("/tmplDemo", demo1)
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		fmt.Println("HTTP server start failed err:", err)
		return
	}
}
