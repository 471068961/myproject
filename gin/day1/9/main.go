package main

import (
	"html/template"
	"net/http"

	"github.com/gin-gonic/gin"
)

// 静态文件：html页面上用到的样式文件， css js 图片
func hand(c *gin.Context) {
	c.HTML(http.StatusOK, "home.html", nil)
}
func main() {
	r := gin.Default()

	// 加载静态文件
	r.Static("xxx", "./statics") //当在模板文件的引入目录结构中发现以 xxx 开头的目录 会在 statics目录下寻找对应的静态文件
	// 给模板添加函数
	r.SetFuncMap(template.FuncMap{
		"safe": func(str string) template.HTML {
			return template.HTML(str)
		},
	})

	// 模板解析
	// r.LoadHTMLFiles("./templates/index.tmpl")

	// 多文件解析
	r.LoadHTMLGlob("templates/**/*")

	r.GET("/posts/index", func(c *gin.Context) {
		// HTTP请求
		c.HTML(http.StatusOK, "posts/index.tmpl", gin.H{ //模板渲染
			"title": "<a href = 'https://www.baidu.com'>百度</a>",
		})
	})

	r.GET("/users/index", func(c *gin.Context) {
		// HTTP请求
		c.HTML(http.StatusOK, "users/index.tmpl", gin.H{ //模板渲染
			"title": "users/index.tmpl",
		})
	})

	// 返回网上下载的模板
	r.GET("/home/home", hand)

	r.Run(":9090") //启动服务
}
