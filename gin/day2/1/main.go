package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	r.GET("/json", func(c *gin.Context) {
		// 方法1：使用map
		// data := map[string]interface{}{
		// 	"name":    "小王子",
		// 	"message": "hello world!",
		// 	"age":     18,
		// }
		// gin.H == map[string]interface{}{}
		data := gin.H{
			"name":    "大王子",
			"message": "hello world",
			"age":     20,
		}
		c.JSON(http.StatusOK, data)
	})

	// 方法二：结构体 灵活使用tag来做定制化操作
	type msg struct {
		Name    string `json:"name"`
		Message string `json:"message"`
		Age     int    `json:"age"`
	}

	r.GET("/another_json", func(c *gin.Context) {
		data := msg{
			Name:    "小王子",
			Message: "Hello Golang",
			Age:     18,
		}
		c.JSON(http.StatusOK, data)
	})

	r.Run(":9090")
}
