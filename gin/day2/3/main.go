package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// 获取form表单提交的参数

func main() {
	r := gin.Default()
	// 解析模板文件
	r.LoadHTMLFiles("./login.html", "./index.html")
	r.GET("/login", func(c *gin.Context) {
		c.HTML(http.StatusOK, "login.html", nil)
	})

	//login post
	r.POST("/login", func(c *gin.Context) {
		// 获取form表单提交的数据
		// 取到返回值，取不到返回空字符串
		// username := c.PostForm("username")
		// password := c.PostForm("password")

		// username := c.DefaultPostForm("username", "游客")
		// password := c.DefaultPostForm("password", "")

		username, ok := c.GetPostForm("username")
		if !ok {
			username = "游客"
		}
		password, ok := c.GetPostForm("password")
		if !ok {
			password = "****"
		}
		c.HTML(http.StatusOK, "index.html", gin.H{
			"Name":     username,
			"PassWord": password,
		})
	})

	r.Run(":9090")
}
