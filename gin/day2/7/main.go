package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()

	r.GET("/index", func(c *gin.Context) {
		// c.JSON(http.StatusOK, gin.H{
		// 	"status": "ok",
		// })

		// 跳转到sogo.com
		c.Redirect(http.StatusMovedPermanently, "https://www.sogo.com/")
	})

	r.GET("/a", func(c *gin.Context) {
		// 跳转到 /b 对应的路由处理器函数
		c.Request.URL.Path = "/b" //修改请求的URI
		r.HandleContext(c)        // 继续后续的处理
	})

	r.GET("/b", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"message": "b",
		})
	})

	r.Run(":9090")
}
