package main

// 中间件
import (
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

func indexHander(c *gin.Context) {
	fmt.Println("index ..")
	name, ok := c.Get("name")
	if !ok {
		name = "匿名用户"
	}
	c.JSON(http.StatusOK, gin.H{
		"msg": name,
	})
}

// 定义一个中间件m1 : 统计请求处理函数的耗时
func m1(c *gin.Context) {
	fmt.Println("m1 in ...")
	// 计时
	start := time.Now()

	// go funcxx(c.Copy()) 只能使用c的拷贝

	c.Next()  //调用后续的函数，即indexHander
	c.Abort() //阻止调用后续的处理函数

	cost := time.Since(start)
	fmt.Println("花费了", cost)
	fmt.Println("m1 out...")
}

func m2(c *gin.Context) {
	fmt.Println("m2 in ...")
	c.Set("name", "Valid")
	// c.Abort() //调用后续的函数，即indexHander
	fmt.Println("m2 out...")
}

// func authMiddleware(c *gin.Context){

// }

func authMiddleware(doCheck bool) gin.HandlerFunc {
	// 连接数据库
	// 或者做一些准备工作
	return func(c *gin.Context) {
		if doCheck {
			//  存放具体的逻辑
			// 	是否登陆的判断中间件
			// 	if 是登陆用户
			c.Next()
			// 	else
			// 	c.Abort()
		} else {
			c.Next()
		}
	}
}

func main() {
	// r := gin.Default() 默认使用了Logger Recovery中间件

	r.New() //创建一个没有默认中间件的路由
	// r.GET("/index", m1, indexHander)
	// r.GET("/user", m1, func(c *gin.Context){
	// 	c.JSON(http.StatusOK, gin.H{
	// 		"mes": "ok",
	// 	})
	// })

	r.Use(m1, m2, authMiddleware(true)) //全局注册中间件
	r.GET("/index", indexHander)
	r.GET("/user", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"mes": "ok",
		})
	})

	// 路由组注册中间件方法1：
	shopGroup := r.Group("/shop", authMiddleware(true))
	{
		shopGroup.GET("/index", func(c *gin.Context) {
			c.JSON(http.StatusOK, gin.H{
				"msg": "shop/index",
			})
		})
	}

	r.Run()
}
