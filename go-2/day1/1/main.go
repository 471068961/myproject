package main

import (
	"go_code/go-2/day1/1/test" //不同包中的init函数按照包导入的先后顺序执行
	"go_code/go-2/day1/1/utils"
)

func main() {

	utils.Count()
	test.GetInit()
}
