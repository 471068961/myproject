package utils

import (
	"fmt"
)

func Count() {
	fmt.Println("utils 包下的Count函数")
}
func init() {
	// init函数可以应用于任意包中，且可以重复定义多个
	fmt.Println("utils包下的init函数，用于初始化一些信息。。。")
}
func init() {
	// 同一个go文件init的执行顺序从上到下
	fmt.Println("utils包下的另一个init函数。。")
}
