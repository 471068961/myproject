package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	// time.Now() 获取当前系统时间
	t1 := time.Now()
	fmt.Println(t1)

	// 获取指定时间
	t2 := time.Date(2020, 2, 17, 12, 45, 0, 0, time.Local)
	fmt.Println(t2)

	// time格式化 模板数据必须固定 2006 1 2 15 04 05
	s1 := t1.Format("2006年1月2日 15:04:05")
	fmt.Println(s1)

	// string --> time
	s2 := "2020/2/17"
	t3, err := time.Parse("2006/1/2", s2)
	if err != nil {
		fmt.Println("转换失败, err:", err)
	} else {
		fmt.Println((t3))
	}

	// 根据当前时间，来获取指定的内容
	year, month, day := t1.Date() //获取年月日
	fmt.Println(year, month, day)

	hour, min, sec := t1.Clock() // 获取时分秒
	fmt.Println(hour, min, sec)

	// 还可以通过Year(), Month(), Day(), Hour(), Minute(), Second(), NanoSecond()
	// 分别获取指定的时间部分
	year2 := t1.Year()
	fmt.Println("年:", year2)
	fmt.Println(t1.YearDay()) //YearDay()方法获取当前年份已过的天数

	// 时间戳：距离1970年1月1日0时0分0秒的时间差值：秒， 纳秒
	t4 := time.Now().Unix() //秒
	fmt.Println(t4)

	t5 := time.Now().UnixNano() //纳秒
	fmt.Println(t5)

	// 时间间隔
	t6 := t1.Add(time.Minute) //当前时间加一分钟
	fmt.Println(t6)

	t7 := t1.AddDate(1, 0, 0) //加上的年月日
	fmt.Println(t7)

	// 时间差值
	d1 := t6.Sub(t1)
	fmt.Println(d1)

	// 睡眠
	time.Sleep(3 * time.Second) //让当前程序进入睡眠状态
	fmt.Println("main out")

	// 随机数
	rand.Seed(time.Now().Unix()) //设置随机数种子
	randNum := rand.Intn(10) + 1
	fmt.Println(randNum)
}
