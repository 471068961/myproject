package main

import (
	"fmt"
	"os"
)

func main() {
	fileInfo, err := os.Stat("./file/a.txt")
	if err != nil {
		fmt.Println("err :", err)
		return
	}

	// 文件名
	fmt.Println(fileInfo.Name())

	// 文件大小
	fmt.Println(fileInfo.Size())

	// 是否为目录
	fmt.Println(fileInfo.IsDir())

	// 修改时间
	fmt.Println(fileInfo.ModTime())
}
