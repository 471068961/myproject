package main

func main() {
	/*
		文件操作：
		1.路径
			相对路径：relative
				从当前main.go文件所在目录开始写
			绝对路径
				从根目录下开始写

				.当前目录
				..上一层
	*/
	// fileName1 := "D:/Valid_Code/Golang/src/go_code/go-2/day2/3/a.txt"
	// fileName2 := "a.txt"

	//检查当前文件路径是否为绝对路径
	// fmt.Println(filepath.IsAbs(fileName1)) //true
	// fmt.Println(filepath.IsAbs(fileName2)) //false

	//获取当前文件的绝对路径
	// fmt.Println(filepath.Abs(fileName1))
	// fmt.Println(filepath.Abs(fileName2))

	// fmt.Println("获取父目录", path.Join(fileName1, ".."))

	//创建目录
	// os.Mkdir 只能创建单层目录
	// err := os.Mkdir("D:/Valid_Code/Golang/src/go_code/go-2/day2/3/test", os.ModePerm)

	// if err != nil {
	// 	fmt.Println("单文件夹创建失败:", err)
	// 	return
	// }
	// fmt.Println("单文件夹创建成功")

	//创建多层目录
	// err = os.MkdirAll("D:/Valid_Code/Golang/src/go_code/go-2/day2/3/aa/bb/cc", os.ModePerm)
	// if err != nil {
	// 	fmt.Println("多文件夹创建失败: ", err)
	// 	return
	// }
	// fmt.Println("多层文件夹创建成功")

	// 创建文件os.Create() 采用模式0666 (任何人都可读写， 不可执行) 创建一个名为name的文件，如果文件以存在会截断它(为空文件)
	// file1, err := os.Create("test/1.txt")
	// if err != nil {
	// 	fmt.Println("文件创建失败", err)
	// }
	// fmt.Println("文件创建成功", file)

	//打开文件 open()打开的文件为只读
	// file2, err := os.Open("test/1.txt")
	// if err != nil {
	// 	fmt.Println("打开文件失败 err", err)
	// 	return
	// }
	// fmt.Println("打开文件成功")
	// defer file2.Close()

	// OpenFile() 第一个参数文件的路径， 第二个参数文件打开方式， 第三个参数文件的模式：文件不存在时创建文件，需要指定权限
	// file4, err := os.OpenFile("a.txt", os.O_RDONLY|os.O_WRONLY, os.ModePerm)
	// if err != nil {
	// 	fmt.Println("文件打开出错 err:", err)
	// 	return
	// }
	// fmt.Println("文件打开成功")
	// defer file4.Close()

	// 删除文件或者文件夹 文件夹只能为空目录 删除非空文件夹RemoveAll
	// 经代码删除的文件不能
	// err := os.Remove("a.txt")
	// if err != nil {
	// 	fmt.Println("文件删除失败", err)
	// 	return
	// }
	// fmt.Println("删除文件成功")
}
