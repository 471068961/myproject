package main

import (
	"fmt"
	"os"
)

func main() {
	file, err := os.OpenFile("1.txt", os.O_RDONLY, os.ModePerm)
	// file, err := os.Open("1.txt")
	if err != nil {
		fmt.Println("文件打开失败 err:", err)
		return
	}
	defer file.Close()

	buf := make([]byte, 4, 4)
	// n, err := file.Read(buf)
	// if err != nil {
	// 	fmt.Println(err)
	// }
	// fmt.Println(string(buf))
	// fmt.Println(n)

	n := -1
	for {
		n, err = file.Read(buf)
		if n == 0 || err != nil {
			fmt.Printf("\n文件读取完毕")
			return
		}
		fmt.Printf("%s", string(buf[:n]))
	}

}
