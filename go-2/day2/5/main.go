package main

import (
	"fmt"
	"os"
)

func main() {
	file, err := os.OpenFile("2.txt", os.O_WRONLY|os.O_CREATE|os.O_APPEND, os.ModePerm)
	if err != nil {
		fmt.Println("打开文件出错", err)
		return
	}
	defer file.Close()

	// 写数据
	buf := []byte{65, 66, 67, 68}
	n, err := file.Write(buf)
	if err != nil {
		fmt.Println("写入数据出错", err)
		return
	}
	fmt.Printf("成功写入了%d个字节\n", n)

	// 写入字符串
	str := "小王子"
	n, err = file.WriteString(str)
	if err != nil {
		fmt.Println("字符串写入失败", err)
		return
	}
	fmt.Printf("成功写入了%d个字节", n)

}
