package main

import (
	"fmt"
	"io"
	"os"
)

// CopyFile 实现了两个文件的赋值
func CopyFile(srcFile, destFile string) (int, error) {
	file1, err := os.Open(srcFile)
	if err != nil {
		return 0, err
	}
	defer file1.Close()

	file2, err := os.OpenFile(destFile, os.O_WRONLY, os.ModePerm)
	if err != nil {
		return 0, err
	}
	defer file2.Close()

	buf := make([]byte, 10, 1024)
	n := -1
	total := 0
	for {
		n, err = file1.Read(buf)
		if err == io.EOF || n == 0 {
			fmt.Println("拷贝完毕")
			break
		} else if err != nil {
			fmt.Println("错误 err :", err)
			return total, err
		}
		total = total + n
		_, err = file2.Write(buf[:n])
		if err != nil {
			return total, err
		}
	}
	return total, nil
}

func main() {
	srcFile := "file/src.txt"
	destFile := "file/dest.txt"
	total, err := CopyFile(srcFile, destFile)
	fmt.Println(total, err)

	// 拷贝文件也可以直接使用io.Copy()
}
