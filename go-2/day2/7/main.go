package main

import (
	"fmt"
	"io"
	"os"
)

func main() {
	file, err := os.OpenFile("test.txt", os.O_RDONLY, os.ModePerm)
	if err != nil {
		fmt.Println("文件打开失败 err", err)
		return
	}

	defer file.Close()

	// 设置文件指针的位置
	_, _ = file.Seek(3, io.SeekStart)
	buf := make([]byte, 1, 1)
	_, err = file.Read(buf)
	if err != nil {
		fmt.Println("文件读取失败, err ", err)
		return
	}

	fmt.Println(string(buf))
}
