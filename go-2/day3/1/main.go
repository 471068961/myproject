package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
)

// 断点续传
// 复制时记录复制的总量

// HandleErr 处理错误
func HandleErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
func main() {
	srcFile := "file/src.txt"
	destFile := srcFile[strings.LastIndex(srcFile, "/")+1:strings.LastIndex(srcFile, ".")] + "(2)" + srcFile[strings.LastIndex(srcFile, "."):]
	fmt.Println(destFile)

	tempFile := destFile + "-temp.txt"
	fmt.Println(tempFile)

	file1, err := os.Open(srcFile)
	HandleErr(err)
	defer file1.Close()

	file2, err := os.OpenFile(destFile, os.O_CREATE|os.O_WRONLY, os.ModePerm)
	HandleErr(err)
	defer file2.Close()

	file3, err := os.OpenFile(tempFile, os.O_CREATE|os.O_RDWR, os.ModePerm)
	HandleErr(err)

	file3.Seek(0, io.SeekStart)
	bs := make([]byte, 100, 100)
	n1, err := file3.Read(bs)
	// HandleErr(err)
	countStr := string(bs[:n1])
	count, err := strconv.ParseInt(countStr, 10, 64)
	// HandleErr(err)

	file1.Seek(count, io.SeekStart)
	file2.Seek(count, io.SeekStart)
	data := make([]byte, 1024, 1024)
	n2 := -1 //读取的数据量
	n3 := -1 //写入的数据量
	total := int(count)

	// 复制文件
	for {
		n2, err = file1.Read(data)
		if err == io.EOF || n2 == 0 {
			fmt.Println("文件复制完毕")
			file3.Close()
			os.Remove(tempFile)
			break
		}
		n3, err = file2.Write(data[:n2])
		total = total + n3
		file3.Seek(0, io.SeekStart)
		file3.WriteString(string(total))
	}

}
