package main

import (
	"bufio"
	"fmt"
	"os"
)
func main(){
	/*
		bufio：高效io读写
			buffer：缓存

		将io包下的Reader, Wrtte对象进行包装，带缓存的包装，提高读写的效率
			ReadBytes()
			ReadString()
			ReadLine()

	*/
	fileName := "test.txt"
	file, err := os.Open(fileName)
	if err != nil {
		fmt.Println(err)
	}
	defer file.Close()

	// 创建Reader对象
	b1 := bufio.NewReader(file)

	// Reade(),高效读取
	// p := make([]byte, 1024)
	// n1, err := b1.Read(p)
	// fmt.Println(string(p[:n1]), n1)

	// Readline()，读取整行 不推荐使用
	data, flag, err := b1.ReadLine()
	fmt.Println(string(data))
	fmt.Println(flag)
	fmt.Println(err)

	// ReadString() 读取一指定界限符的字符串
	s1, err := b1.ReadString('\n')
	fmt.Println(s1)

	// 读取键盘输入
	b2 := bufio.NewReader(os.Stdin)
	s2, _ := b2.ReadString('\n')
	fmt.Println(s2)

	str := ""
	fmt.Scanf("%s", &str)
	fmt.Println(str)
}