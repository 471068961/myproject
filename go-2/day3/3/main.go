package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	fileName := "test.txt"
	file, err := os.OpenFile(fileName, os.O_RDWR|os.O_CREATE, os.ModePerm)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close()

	w1 := bufio.NewWriter(file)
	n, err := w1.WriteString("Hello World")
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Printf("写入了%d个字节", n)
	// 刷新缓冲区，把缓冲区中的内容写入文件
	w1.Flush()
}
