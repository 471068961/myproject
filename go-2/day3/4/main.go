package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

func main() {
	/*
		ioutil包
			ReadFile()
			WriteFile()
			DeadDir()
	*/

	//读取文件中的所有的数据
	fileName := "test.txt"
	data, err := ioutil.ReadFile(fileName)
	fmt.Println(err)
	fmt.Println(string(data))

	err = ioutil.WriteFile(fileName, []byte("面朝大海，春暖花开"), os.ModePerm)
	fmt.Println(err)
}
