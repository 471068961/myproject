package main

import (
	"fmt"
	"io/ioutil"
	"log"
)

func main() {
	/*

		遍历文件夹
	*/
	dirName := "D:/Valid_Code/Golang/src/go_code"
	listFiles(dirName, 0)
}

func listFiles(dirName string, level int) {
	// level 用来记录当前递归的层次，生成带有层次感的空格
	s := "|——"
	for i := 0; i < level; i++ {
		s = "|  " + s
	}
	fileInfos, err := ioutil.ReadDir(dirName)
	if err != nil {
		log.Fatal(err)
	}
	for _, fi := range fileInfos {
		filename := dirName + "/" + fi.Name()
		fmt.Printf("%s%s\n", s, filename)
		if fi.IsDir() {
			listFiles(filename, level+1)
		}
	}
}
