package main

import (
	"fmt"
	"time"
)

func printNum() {
	for i := 1; i <= 1000; i++ {
		fmt.Printf("子goroutine中打印数字：%d\n", i)
	}
}
func main() {
	// 先创建并启动子goroutine
	go printNum()

	for i := 1; i <= 1000; i++ {
		fmt.Printf("\t主goroutine中打印字母: A\n")
	}
	time.Sleep(time.Second)
}
