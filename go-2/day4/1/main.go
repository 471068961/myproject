package main

import (
	"fmt"
	"runtime"
	"time"
)

func init() {
	// 设置go执行的最大CPU总数一般放在init函数中
	// 设置go程序执行的最大的CPU的数量：[1, 256] 推荐填写当前电脑的CPU总数
	n := runtime.GOMAXPROCS(runtime.NumCPU())
	fmt.Println(n)
}
func main() {
	//获取GOROOT安装目录
	fmt.Println("GOROOT -->", runtime.GOROOT()) //GOROOT --> D:\App\go1.13.4.windows-amd64\go

	//获取操作系统
	fmt.Println("os", runtime.GOOS) //os windows

	//获取逻辑CPU的数量
	fmt.Println("逻辑CPU的数量", runtime.NumCPU()) //逻辑CPU的数量 8

	// gosched
	// go func() {
	// 	for i := 0; i < 5; i++ {
	// 		fmt.Println("goroutine...")
	// 	}
	// }()

	// for i := 0; i < 4; i++ {

	// 	// 让出时间片，先让别的goroutine执行
	// 	runtime.Gosched()
	// 	fmt.Println("main...")
	// }

	// 创建goroutine
	go func() {
		fmt.Println("goroutine 开始。。。")
		fun()
		fmt.Println("goroutine结束。。。")
	}()
	time.Sleep(time.Second)
}

func fun() {
	defer fmt.Println("defer ...")
	// 中断当前goroutine 但defer语句会被执行
	runtime.Goexit()
}
