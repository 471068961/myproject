package main

import (
	"fmt"
)

func sendData(ch chan string) {
	for i := 0; i < 10; i++ {
		ch <- fmt.Sprintf("数据%d", i)
	}
	close(ch)
}
func main() {
	// 定义带缓冲的通道
	// make(chan T, cap) 仅当缓冲区的数据满了，或者空了才会阻塞

	ch := make(chan string, 4)
	go sendData(ch)

	for {
		v, ok := <-ch
		if !ok {
			fmt.Println("数据读取完毕")
			break
		} else {
			fmt.Println(v)
		}
	}
}
