package main

import (
	"fmt"
)

// 单向channel
// 在创建channel时创建双向，而在具体函数调用时则根据情况确定
func main() {
	// ch1 := make(chan <- int) //单向，只能写不能读
	// ch2 := make(<- chan int) //单向，只能读不能写

	ch3 := make(chan int)

	// go fun1(ch3)
	// data := <-ch3
	// fmt.Println(data)

	go fun1(ch3)
	go fun2(ch3)
	ch3 <- 1000
	fmt.Println("main over")
}

// 只能操作只写的通道
func fun1(ch chan<- int) {
	ch <- 100
	fmt.Println("fun1 函数结束")
}

// 只能操作只读的通道
func fun2(ch <-chan int) {
	data := <-ch
	fmt.Println(data)
}
