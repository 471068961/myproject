package main

import (
	"fmt"
	"time"
)

func main() {
	// 创建一个计时器 3秒以后触发
	// timer := time.NewTimer(3 * time.Second)
	// fmt.Println(time.Now())
	// 三秒钟后才会触发接下来的动作
	// ch2 := timer.C
	// fmt.Println(<-ch2)

	// 新建一个计时器
	timer2 := time.NewTimer(5 * time.Second)
	// 开始goroutine
	go func() {
		<-timer2.C
		fmt.Println("Timer2 结束了。")
	}()

	time.Sleep(3 * time.Second)
	flag := timer2.Stop()
	if flag {
		fmt.Println("Timer2 停止了")
	}
}
