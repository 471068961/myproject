package main

import (
	"fmt"
	"time"
)

func main() {
	// time.After() 返回一个 chan Time 粗出的时d时间间隔之后的当前时间
	ch := time.After(3 * time.Second)
	fmt.Println(time.Now())

	time2 := <-ch
	fmt.Println(time2)
}
