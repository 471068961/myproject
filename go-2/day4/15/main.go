package main

import (
	"fmt"
	"time"
)

func main() {
	ch1 := make(chan int)
	ch2 := make(chan int)

	go func() {
		ch1 <- 100
	}()
	select {
	case <-ch1:
		fmt.Println("case 1")
	case <-ch2:
		fmt.Println("case 2")
	// 如果没有default则会执行case3
	case <-time.After(3 * time.Second):
		fmt.Println("timeout")
	default:
		fmt.Println("default")
	}
}
