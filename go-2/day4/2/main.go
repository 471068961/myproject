package main

import (
	"fmt"
	"time"
)

func main() {
	// 临界资源  在不同的goroutine中访问相同的资源
	a := 1
	go func() {
		a = 2
		fmt.Println("goroutine中。。", a)
	}()
	a = 3
	time.Sleep(time.Second)
	fmt.Println("main ", a)

}
