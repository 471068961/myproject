package main

import (
	"fmt"
	"time"
)

var ticket = 100 //100张票
func main() {
	// 模拟4个火车站售票口
	go saleTickets("售票口1")
	go saleTickets("售票口2")
	go saleTickets("售票口3")
	go saleTickets("售票口4")
	time.Sleep(time.Second)
}

func saleTickets(name string) {
	for {
		if ticket > 0 {
			fmt.Println(name, "售出", ticket)
			ticket--
		} else {
			fmt.Println(name, "没有票了")
			break
		}
	}
}
