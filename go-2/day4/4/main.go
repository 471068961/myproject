package main

import (
	"fmt"
	"sync"
)

var wg sync.WaitGroup //创建同步等待组对象
func main() {
	// WaitGroup: 同步等待组

	// 设置等待组中要执行的子goroutine的数量
	wg.Add(2)

	go func1()
	go func2()

	fmt.Println("main函数阻塞")
	wg.Wait() //等待子goroutine执行完毕
	fmt.Println("main out")
}

func func1() {
	for i := 1; i < 10; i++ {
		fmt.Println("func1()函数打印。。A\n", i)
	}
	wg.Done() //给wg等待组中的counter数值减1
}

func func2() {
	defer wg.Done()
	for i := 1; i < 10; i++ {
		fmt.Printf("\tfunc2()函数打印。。%d\n", i)
	}
}
