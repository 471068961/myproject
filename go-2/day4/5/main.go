package main

import (
	"fmt"
	"sync"
)

// 通过同步锁来解决临界资源的问题
var mutex sync.Mutex //定义锁的对象

var wg sync.WaitGroup
var ticket = 100 //100张票
func main() {
	// 模拟4个火车站售票口
	wg.Add(4)
	go saleTickets("售票口1")
	go saleTickets("售票口2")
	go saleTickets("售票口3")
	go saleTickets("售票口4")
	wg.Wait()
}

func saleTickets(name string) {
	for {
		//上锁 同步锁上锁一定要解锁，否则会发生死锁等问题
		mutex.Lock()
		if ticket > 0 {

			fmt.Println(name, "售出", ticket)
			ticket--
		} else {
			mutex.Unlock()
			fmt.Println(name, "没有票了")
			break
		}
		mutex.Unlock()
	}
	wg.Done()
}
