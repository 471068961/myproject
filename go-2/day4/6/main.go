package main

import (
	"fmt"
	"sync"
	"time"
)

var rwMutex *sync.RWMutex

var wg sync.WaitGroup

func main() {
	wg.Add(2)
	rwMutex = new(sync.RWMutex)

	// 读操作可以同时进行
	// go readData(1)
	// go readData(2)

	go writeData(1)
	go writeData(2)

	wg.Wait()
}

func writeData(i int) {
	defer wg.Done()
	fmt.Println(i, "开始写：write start。。")

	// 写操作上锁 写锁一旦上锁便不能进行其他goroutine的读写锁的操作
	rwMutex.Lock()
	fmt.Println(i, "正在写：writing。。。")
	time.Sleep(time.Second)
	rwMutex.Unlock()

	fmt.Println(i, "写操作结束")
}

func readData(i int) {
	defer wg.Done()
	fmt.Println(i, "开始读：read start。。")

	rwMutex.RLock() //读操作上锁
	fmt.Println(i, "正在读取数据：reading")
	time.Sleep(time.Second)
	rwMutex.RUnlock() //读操作解锁

	fmt.Println(i, "读结束read over。。")
}
