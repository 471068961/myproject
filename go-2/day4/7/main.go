package main

import (
	"fmt"
)

// channel 通道
func main() {
	// chan是引用类型
	// var a chan int
	// fmt.Printf("%T\n%v\n", a, a)

	var ch1 chan bool
	ch1 = make(chan bool)

	go func() {
		for i := 0; i < 10; i++ {
			fmt.Println("子gorutine。。", i)
		}
		fmt.Println("子goroutine over")
		ch1 <- true
	}()
	data := <-ch1
	fmt.Println("main goroutine", data)
	fmt.Println("main over")
}
