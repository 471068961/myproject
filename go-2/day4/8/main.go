package main

import (
	"fmt"
)

func main() {
	ch1 := make(chan int) //没有缓存机制的chan必须读写同时具备否则会发生死锁

	go func() {
		data := <-ch1
		fmt.Println("goroutine 。。", data)

	}()

	ch1 <- 10
	fmt.Println("main over")

	// ch2 := make(chan int)
	// ch2 <- 5
	// 以上两行代码将会发生死锁
}
