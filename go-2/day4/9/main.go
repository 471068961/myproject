package main

import (
	"fmt"
)

func sendData(ch chan int) {
	for i := 0; i < 10; i++ {
		ch <- i + 1
	}
	close(ch) //通知接收方，通道关闭
}
func main() {
	ch := make(chan int)
	go sendData(ch)

	// 第一种读取管道的方式
	// for {
	// 	v, ok := <-ch
	// 	if !ok {
	// 		fmt.Println("数据已经全部读取完毕")
	// 		break
	// 	} else {
	// 		fmt.Println("读取的数据为", v)
	// 	}
	// }

	// 第二种读取管道的方式
	for v := range ch {
		fmt.Println(v)
	}

}
