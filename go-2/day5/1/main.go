package main

import (
	"fmt"
	"reflect"
)

func main() {
	// 反射操作：通过反射，可以获取一个接口类型变量的 类型和数值

	var x float64 = 3.4

	fmt.Println(reflect.TypeOf(x))  // reflect.TypeOf() 获取当前变量的类型
	fmt.Println(reflect.ValueOf(x)) // reflect.ValueOf() 获取当前变量的类型
}
