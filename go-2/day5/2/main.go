package main

import (
	"fmt"
	"reflect"
)

func main() {
	var num float64 = 1.23

	// 接口变量 --> 反射类型对象
	value := reflect.ValueOf(num)

	// 反射类型对象 --> 接口类型变量
	convertValue := value.Interface().(float64)
	fmt.Println(convertValue)

	// 反射类型对象 --> 接口类型变量，理解为 "强制转换"
	pointer := reflect.ValueOf(&num)
	covertPointer := pointer.Interface().(*float64)
	fmt.Println(covertPointer)
}
