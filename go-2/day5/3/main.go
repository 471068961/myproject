package main

import (
	"fmt"
	"reflect"
)

// Person 表示人
type Person struct {
	Name string
	Age  int
	Sex  string
}

// Say 表示说话
func (p Person) Say(msg string) {
	fmt.Println("hello", msg)
}

// PrintInfo 打印用户信息
func (p Person) PrintInfo() {
	fmt.Printf("姓名：%s, 年龄：%d, 性别：%s\n", p.Name, p.Age, p.Sex)
}

func main() {
	p1 := Person{
		Name: "王二狗",
		Age:  18,
		Sex:  "男",
	}
	GetMessage(p1)
}

// GetMessage 获取input的信息
func GetMessage(input interface{}) {
	getType := reflect.TypeOf(input)             //先获取input的类型
	fmt.Println("get Type is", getType.Name())   //类型名字Person
	fmt.Println("get Kind is: ", getType.Kind()) //类型种类struct

	// getValue := reflect.ValueOf(input) //获取所有字段的数值
	// fmt.Println("get all Fields is:", getValue)

	// 获取字段信息
	// for i := 0; i < getType.NumField(); i++ {
	// 	field := getType.Field(i)
	// 	value := getValue.Field(i).Interface()
	// 	fmt.Printf("字段的名称%s, 字段的类型%v, 字段的值为%v\n ", field.Name, field.Type, value)
	// }

	fmt.Println(getType.NumMethod())
	// 获取方法
	for i := 0; i < getType.NumMethod(); i++ {
		method := getType.Method(i)
		fmt.Printf("方法名称：%s, 方法类型：%v\n", method.Name, method.Type)
	}
}
