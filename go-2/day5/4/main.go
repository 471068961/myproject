package main

import (
	"fmt"
	"reflect"
)

func main() {
	var num float64 = 1.2

	// 通过反射来修改具体变量的值，参数必须是指针
	pointer := reflect.ValueOf(&num)

	newValue := pointer.Elem() //获取value

	fmt.Println("是否可以修改变量的值: ", newValue.CanSet())

	newValue.SetFloat(3.2)

	fmt.Println(num)
}
