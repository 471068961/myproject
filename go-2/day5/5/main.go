package main

import (
	"fmt"
	"reflect"
)

type Student struct {
	Name   string
	Age    int
	School string
}

func main() {
	s1 := Student{
		Name:   "程序员",
		Age:    16,
		School: "天堂学院",
	}

	value := reflect.ValueOf(&s1)

	newValue := value.Elem()

	f1 := newValue.FieldByName("Name")
	f1.SetString("haha")

	fmt.Println(s1)
}
