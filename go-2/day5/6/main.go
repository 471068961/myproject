package main

import (
	"fmt"
	"reflect"
)

// 反射调用方法

// Person 表示人
type Person struct {
	Name string
	Age  int
	Sex  string
}

// Say 表示说话
func (p Person) Say(msg string) {
	fmt.Println("hello", msg)
}

// PrintInfo 打印用户信息
func (p Person) PrintInfo() {
	fmt.Printf("姓名：%s, 年龄：%d, 性别：%s\n", p.Name, p.Age, p.Sex)
}

func main() {
	p1 := Person{
		Name: "王二狗",
		Age:  18,
		Sex:  "男",
	}

	value := reflect.ValueOf(p1)

	fmt.Println(value.Kind(), value.Type())

	methodValue := value.MethodByName("PrintInfo")
	fmt.Println(methodValue.Kind(), methodValue.Type())

	methodValue.Call(nil) //调用结构体方法时，可以使用nil或者传递make([]reflect.Value, 0)

	// args := make([]reflect.Value, 0)
	// methodValue.Call(args)

	methodValue2 := value.MethodByName("Say")
	fmt.Println(methodValue2.Kind(), methodValue2.Type())

	args2 := []reflect.Value{reflect.ValueOf("haha")}
	methodValue2.Call(args2)
}
