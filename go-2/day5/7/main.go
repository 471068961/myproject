package main

import (
	"fmt"
	"reflect"
	"strconv"
)

// 反射调用函数

func fun1() {
	fmt.Println("我是函数fun1，无参")
}

func fun2(i int, s string) {
	fmt.Println("我是函数fun2，有参的")
}

func fun3(i int, s string) string {
	fmt.Println("我是fun3有返回值")
	return s + strconv.Itoa(i)
}
func main() {
	f1 := fun1
	value := reflect.ValueOf(f1)
	fmt.Printf("kind %s, Type %s\n", value.Kind(), value.Type())

	value2 := reflect.ValueOf(fun2)
	value2.Call([]reflect.Value{reflect.ValueOf(100), reflect.ValueOf("haha")})

	value3 := reflect.ValueOf(fun3)
	resultValue := value3.Call([]reflect.Value{reflect.ValueOf(100), reflect.ValueOf("golang")})

	s := resultValue[0].Interface().(string)
	fmt.Println(s)
	// for i, v := range resultValue {
	// 	fmt.Println(i, v) 此时的v为reflect.Value类型
	// }

}
