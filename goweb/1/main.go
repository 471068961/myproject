package main

import (
	"fmt"
	"net/http"
)

// w, 给客户端恢复数据
// r, 读取客户端发送的数据
func HandConn(w http.ResponseWriter, r *http.Request) {
	fmt.Println("r.Method = ", r.Method) //请求方法

	fmt.Println("r.URL = ", r.URL) //URL

	fmt.Println("r.Header = ", r.Header) //请求头

	fmt.Println("r.Body = ", r.Body) //请求体

	w.Write([]byte("hello go"))
}

func main() {
	// 创建路由
	http.HandleFunc("/", HandConn)

	http.ListenAndServe(":8000", nil)
}
