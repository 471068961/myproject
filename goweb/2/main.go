package main

import (
	"fmt"
	"net/http"
	"os"
)

func main() {
	// r := gin.Default()
	// r.GET("www.baidu.com", func(c *gin.Context) {
	// 	fmt.Println("haha", c.Request.Body)
	// })

	r, err := http.Get("https://www.baidu.com/")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer r.Body.Close()

	fileName := "哔哩哔哩.html"
	file, err := os.Create(fileName)
	defer file.Close()
	if err != nil {
		fmt.Println(err)
		return
	}

	buf := make([]byte, 1024*4)
	for {
		n, err := r.Body.Read(buf)
		if n == 0 {
			fmt.Println("return err = ", err)
			fmt.Println("爬取完成")
			return
		}
		_, _ = file.WriteString(string(buf[:n]))
	}
}
