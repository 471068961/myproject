package main

import (
	"fmt"
	"net/http"
	"os"
	"strconv"
)

func httpGet(url string) (result string, err error) {
	// 向服务器发送请求
	r, err := http.Get(url)
	if err != nil {
		return
	}
	defer r.Body.Close()

	buf := make([]byte, 1024*4)
	for {
		n, err := r.Body.Read(buf)
		if n == 0 {
			fmt.Println("r.Body.Read failed", err)
			break
		}
		result = result + string(buf[:n])
	}
	return
}

func DoWork(start, end int) {
	fmt.Printf("正在爬取 %d 到 %d 的页面\n", start, end)
	for i := start; i <= end; i++ {
		url := "https://tieba.baidu.com/f?kw=golang&ie=utf-8&pn=" + strconv.Itoa((i-1)*50)
		fmt.Println("url =", url)

		// 爬取网站 将网站的整个页面爬取
		result, err := httpGet(url)
		if err != nil {
			fmt.Println("err :", err)
			continue
		}

		// 把内容写入文件
		fileName := strconv.Itoa(i) + ".html"

		file, err := os.Create(fileName)
		if err != nil {
			fmt.Println("create file failed, err :", err)
			continue
		}

		file.WriteString(result)
		file.Close()
	}
}
func main() {
	var start, end int
	fmt.Println("请输入起始页(>=1):")
	fmt.Scan(&start)
	fmt.Println("请输入终止页(>=起始页):")
	fmt.Scan(&end)
	DoWork(start, end)
}
